#!/usr/bin/env bash
set -e
if [ ! -d "/root/.vim/dein" ]; then
    cd /root/.vim
    ./install.sh
fi
exec "$@"
